﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senparc.Areas.Admin.Domain.Dto
{
    /// <summary>
    /// 卡扣人员数据管理
    /// </summary>
    public class ClipUserDto
    {
        public int Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
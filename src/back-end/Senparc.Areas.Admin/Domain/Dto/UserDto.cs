﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senparc.Areas.Admin.Domain.Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public int UserName { get; set; }
        public string Password { get; set; }
    }
}
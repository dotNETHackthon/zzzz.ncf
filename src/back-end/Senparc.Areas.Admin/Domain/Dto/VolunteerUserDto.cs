﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senparc.Areas.Admin.Domain.Dto
{
    /// <summary>
    /// 志愿者人员
    /// </summary>
    public class VolunteerUserDto
    {
        public int Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Senparc.Areas.Admin.Domain.Models
{
    [Table(Register.DATABASE_PREFIX + nameof(UserInfo) + "s")]//必须添加前缀，防止全系统中发生冲突
    [Serializable]
    public class UserInfo
    {
        public int Id { get; set; }
        public int UserName { get; set; }
        public string Password { get; set; }
    }
}
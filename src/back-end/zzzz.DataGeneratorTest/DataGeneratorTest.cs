using System;
using System.Linq;
using Xunit;
using zzzz.Xncf.EpidemicManagement.Domain.Services;

namespace zzzz.DataGeneratorTest;

public class DataGeneratorTest
{
    private enum SomeEnum { A, B, C }
    private class Person { public string Name { get; set; } public int Age { get; set; } }

    [Fact]
    public void demo()
    {
        var randomString = Generator.RandomString(100);
            
        var randomInt1 = Generator.RandomInt();
        var randomInt2 = Generator.RandomInt(100);
        var randomInt3 = Generator.RandomInt(-2, 2);
            
        var randomLong1 = Generator.RandomLong();
        var randomLong2 = Generator.RandomLong(100);
        var randomLong3 = Generator.RandomLong(-2, 2);
            
        var randomDate = Generator.RandomDate();
            
        var randomDouble1 = Generator.RandomDouble();
        var randomDouble2 = Generator.RandomDouble(2, 100);
            
        var randomEnum = Generator.RandomEnum<SomeEnum>();    
            
        var person = Generator.RandomObject<Person>();
        var personList = Generator.RandomList<Person>(2); Assert.Equal(2, personList.Count);
        var person2 = Generator.RandomObject<Person>().Build(t => t.Age = 1).Build(t => t.Name = "Tom");
        var personList2 = Generator.RandomList<Person>().Select(t => t.Age = 1).ToList();
            
        var range1 = Generator.Range(10);
        var range2 = Generator.Range(2, 8);
        var range3 = Generator.Range(1, 31, t => new DateTime(2020, 1, t));
        Generator.Range(10, t => Console.WriteLine(t));
        Generator.Range(2, 8, t => Console.WriteLine(t));
            
        var list1 = 1.AsList();
        var list2 = Generator.RandomInt().AsList();
    }
}
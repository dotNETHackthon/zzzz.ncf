using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Senparc.Ncf.AreaBase.Admin;
using Senparc.Ncf.AreaBase.Admin.Filters;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.Service;
using Senparc.Ncf.Utility;
using zzzz.Xncf.CommunityManagement;
using zzzz.Xncf.CommunityManagement.OHS.Local.AppService;

namespace zzzz.Xncf.CommunityManagement.Areas.Admin.Pages.CommunityInfo
{
    [IgnoreAntiforgeryToken]
    public class Index : AdminXncfModulePageModelBase
    {
        private readonly CommunityInfoAppService _communityInfoService;
        // public PagedList<StreetDto> Streets { get; set; }

        public Index(Lazy<XncfModuleService> xncfModuleService, CommunityInfoAppService communityInfoService) : base(xncfModuleService)
        {
            _communityInfoService = communityInfoService;
        }


        public async Task<IActionResult> OnGetListAsync(string adminUserInfoName, int pageIndex, int pageSize)
        {
            var admins = await _communityInfoService.GetOrInitStreetDtoAsync();
            return Ok(new { admins.TotalCount, admins.PageIndex, List = admins.AsEnumerable() });
        }
    }
}
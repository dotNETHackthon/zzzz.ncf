﻿using Senparc.Ncf.Service;
using System;

namespace zzzz.Xncf.CommunityManagement.Areas.CommunityManagement.Pages
{
    public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
    {
        public Index(Lazy<XncfModuleService> xncfModuleService) : base(xncfModuleService)
        {

        }

        public void OnGet()
        {
        }
    }
}

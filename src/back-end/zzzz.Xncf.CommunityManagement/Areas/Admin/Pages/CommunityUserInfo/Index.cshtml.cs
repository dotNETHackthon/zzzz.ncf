using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.AreaBase.Admin;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.Service;
using Senparc.Ncf.Utility;
using zzzz.Xncf.CommunityManagement.OHS.Local.AppService;

namespace zzzz.Xncf.CommunityManagement.Areas.Admin.Pages.CommunityUserInfo
{
    [IgnoreAntiforgeryToken]
    public class Index : AdminXncfModulePageModelBase
    {
        private readonly CommunityUserInfoAppService _communityInfoService;
        // public PagedList<StreetDto> Streets { get; set; }

        public Index(Lazy<XncfModuleService> xncfModuleService, CommunityUserInfoAppService communityInfoService) : base(xncfModuleService)
        {
            _communityInfoService = communityInfoService;
        }


        public async Task<IActionResult> OnGetListAsync(string adminUserInfoName, int pageIndex, int pageSize)
        {
            var admins = await _communityInfoService.GetOrInitStreetDtoAsync();
            return Ok(new { admins.TotalCount, admins.PageIndex, List = admins.AsEnumerable() });
        }
    }
}
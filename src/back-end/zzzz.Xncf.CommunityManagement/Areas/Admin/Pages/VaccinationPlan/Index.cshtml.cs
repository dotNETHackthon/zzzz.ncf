using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Senparc.Ncf.AreaBase.Admin;
using Senparc.Ncf.Service;
using zzzz.Xncf.CommunityManagement.Domain.Services;
using zzzz.Xncf.CommunityManagement.OHS.Local.AppService;

namespace zzzz.Xncf.CommunityManagement.Areas.Admin.Pages.VaccinationPlan
{
    [IgnoreAntiforgeryToken]
    public class Index : AdminXncfModulePageModelBase
    {
        private readonly VaccinationPlanInfoAppService _communityInfoService;

        public Index(Lazy<XncfModuleService> xncfModuleService, VaccinationPlanInfoAppService communityInfoService) : base(xncfModuleService)
        {
            _communityInfoService = communityInfoService;
        }


        public async Task<IActionResult> OnGetListAsync(string adminUserInfoName, int pageIndex, int pageSize)
        {
            var admins = await _communityInfoService.GetOrInitStreetDtoAsync();
            return Ok(new { admins.TotalCount, admins.PageIndex, List = admins.AsEnumerable() });
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.AreaBase.Admin;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.Service;
using Senparc.Ncf.Utility;
using zzzz.Xncf.CommunityManagement.OHS.Local.AppService;

namespace zzzz.Xncf.CommunityManagement.Areas.Admin.Pages.VaccinationPoint
{
    [IgnoreAntiforgeryToken]
    public class Index : AdminXncfModulePageModelBase
    {
        private readonly VaccinationPointInfoAppService _communityInfoService;

        public Index(Lazy<XncfModuleService> xncfModuleService, VaccinationPointInfoAppService communityInfoService) : base(xncfModuleService)
        {
            _communityInfoService = communityInfoService;
        }


        public async Task<IActionResult> OnGetListAsync(string adminUserInfoName, int pageIndex, int pageSize)
        {
            var admins = await _communityInfoService.GetOrInitStreetDtoAsync();
            return Ok(new { admins.TotalCount, admins.PageIndex, List = admins.AsEnumerable() });
        }
    }
}
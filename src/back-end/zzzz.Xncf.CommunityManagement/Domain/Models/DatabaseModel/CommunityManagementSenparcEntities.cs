﻿using Microsoft.EntityFrameworkCore;
using Senparc.Ncf.Database;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.XncfBase.Database;

namespace zzzz.Xncf.CommunityManagement.Models
{
    public class CommunityManagementSenparcEntities : XncfDatabaseDbContext
    {
        public CommunityManagementSenparcEntities(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
        }



        //DOT REMOVE OR MODIFY THIS LINE 请勿移除或修改本行 - Entities Point
        //ex. public DbSet<Color> Colors { get; set; }


        public DbSet<CommunityInfo> CommunityInfo { get; set; }
        public DbSet<CommunityUserInfo> CommunityUserInfo { get; set; }
        public DbSet<VaccinationPlanInfo> VaccinationPlanInfo { get; set; }
        public DbSet<VaccinationPointInfo> VaccinationPointInfo { get; set; }
        public DbSet<VaccineBatchesInfo> VaccineBatchesInfo { get; set; }


        //如无特殊需需要，OnModelCreating 方法可以不用写，已经在 Register 中要求注册
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //}
    }
}

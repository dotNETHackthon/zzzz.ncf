﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    ///  社区信息管理
    /// </summary>
    public class CommunityInfoDto
    {
        public CommunityInfoDto() { }

        public int Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

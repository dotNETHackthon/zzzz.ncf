﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 社区信息用户管理
    /// </summary>
    public class CommunityUserInfoDto
    {
        public CommunityUserInfoDto() { }

        public int Id { get; set; }
        public string CommunityName { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
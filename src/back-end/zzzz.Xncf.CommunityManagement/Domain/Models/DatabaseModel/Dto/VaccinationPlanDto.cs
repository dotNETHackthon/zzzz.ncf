﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 接种计划管理
    /// </summary>
    public class VaccinationPlanDto
    {
        public VaccinationPlanDto() { }

        public int Id { get; set; }

        public string PlanName { get; set; }
        public string PlanCount { get; set; }

        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }

        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 接种点管理
    /// </summary>
    public  class VaccinationPointDto
    {
        public VaccinationPointDto() { }

        public int Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

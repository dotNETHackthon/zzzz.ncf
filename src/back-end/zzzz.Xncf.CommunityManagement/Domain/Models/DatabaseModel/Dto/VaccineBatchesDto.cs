﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto
{
    /// <summary>
    /// 接种批次管理
    /// </summary>
    public  class VaccineBatchesDto
    {
        public VaccineBatchesDto() { }

        public int Id { get; set; }
        public string BatchNo { get; set; }
        public string Remark { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
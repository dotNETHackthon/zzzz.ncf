﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Mapping
{
    public class CommunityManagement_CommunityInfoConfigurationMapping : ConfigurationMappingWithIdBase<CommunityInfo, int>
    {
        public override void Configure(EntityTypeBuilder<CommunityInfo> builder)
        {
        }
    }
}

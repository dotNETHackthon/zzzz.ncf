﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class CommunityManagement_VaccineBatchesInfoConfigurationMapping : ConfigurationMappingWithIdBase<VaccineBatchesInfo, int>
    {
        public override void Configure(EntityTypeBuilder<VaccineBatchesInfo> builder)
        {
        }
    }
}

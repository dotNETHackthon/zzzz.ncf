﻿using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Models
{
    [Table(Register.DATABASE_PREFIX + nameof(VaccineBatchesInfo) + "s")]
    [Serializable]
    public class VaccineBatchesInfo : EntityBase<int>
    {
        //public int Id { get; set; }
        public string BatchNo { get; set; }

        public string Remark { get; set; }
        public string Creator { get; set; }
        public DateTime CreateTime { get; set; }

        public int Flag { get; set; }
        public DateTime AddTime { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public int TenantId { get; set; }
        public string AdminRemark { get; set; }
    }
}
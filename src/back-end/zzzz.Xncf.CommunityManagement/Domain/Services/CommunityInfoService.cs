﻿using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Services
{
    public class CommunityInfoService : ServiceBase<CommunityInfo>
    {
        public CommunityInfoService(IRepositoryBase<CommunityInfo> repo, IServiceProvider serviceProvider)
             : base(repo, serviceProvider)
        {
        }

        public async Task<CommunityInfoDto> OnGetListAsync(CommunityInfo street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            CommunityInfoDto communityInfoDto = base.Mapper.Map<CommunityInfoDto>(street);
            return communityInfoDto;
        }
    }
}
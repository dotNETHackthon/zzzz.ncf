﻿using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Services
{
    public class CommunityUserInfoService : ServiceBase<CommunityUserInfo>
    {
        public CommunityUserInfoService(IRepositoryBase<CommunityUserInfo> repo, IServiceProvider serviceProvider)
             : base(repo, serviceProvider)
        {
        }

        public async Task<CommunityUserInfoDto> OnGetListAsync(CommunityUserInfo street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            CommunityUserInfoDto communityInfoDto = base.Mapper.Map<CommunityUserInfoDto>(street);
            return communityInfoDto;
        }
    }
}

﻿using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Services
{
    public class VaccinationPlanInfoService : ServiceBase<VaccinationPlanInfo>
    {
        public VaccinationPlanInfoService(IRepositoryBase<VaccinationPlanInfo> repo, IServiceProvider serviceProvider)
             : base(repo, serviceProvider)
        {
        }

        public async Task<VaccinationPlanDto> OnGetListAsync(VaccinationPlanInfo street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            VaccinationPlanDto communityInfoDto = base.Mapper.Map<VaccinationPlanDto>(street);
            return communityInfoDto;
        }
    }
}

﻿using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Services
{
    public class VaccinationPointInfoService : ServiceBase<VaccinationPointInfo>
    {
        public VaccinationPointInfoService(IRepositoryBase<VaccinationPointInfo> repo, IServiceProvider serviceProvider)
             : base(repo, serviceProvider)
        {
        }

        public async Task<VaccinationPointDto> OnGetListAsync(VaccinationPointInfo street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            VaccinationPointDto communityInfoDto = base.Mapper.Map<VaccinationPointDto>(street);
            return communityInfoDto;
        }
    }
}
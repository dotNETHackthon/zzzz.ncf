﻿using Microsoft.AspNetCore.Http;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Models;

namespace zzzz.Xncf.CommunityManagement.Domain.Services
{
    public class VaccineBatchesService : ServiceBase<VaccineBatchesInfo>
    {
        public VaccineBatchesService(IRepositoryBase<VaccineBatchesInfo> repo, IServiceProvider serviceProvider)
             : base(repo, serviceProvider)
        {
        }

        public async Task<VaccinationPointDto> OnGetListAsync(VaccineBatchesInfo street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            VaccinationPointDto communityInfoDto = base.Mapper.Map<VaccinationPointDto>(street);
            return communityInfoDto;
        }
    }
}
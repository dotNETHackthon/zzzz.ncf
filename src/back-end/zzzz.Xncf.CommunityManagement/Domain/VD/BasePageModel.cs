﻿using Senparc.Ncf.Core.Models.VD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.Domain.VD
{
    public interface IBasePageModel : IPageModelBase
    { }

    /// <summary>
    /// 当前项目供前端（非Areas）使用的PageModel全局基类
    /// </summary>
    public class BasePageModel : PageModelBase, IBasePageModel
    {
    }
}
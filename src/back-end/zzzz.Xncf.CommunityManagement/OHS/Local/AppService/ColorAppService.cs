﻿using Senparc.CO2NET;
using Senparc.Ncf.Core.AppServices;
using zzzz.Xncf.CommunityManagement.Domain.Services;
using zzzz.Xncf.CommunityManagement.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class ColorAppService : AppServiceBase
    {
        private ColorService _colorService;
        public ColorAppService(IServiceProvider serviceProvider, ColorService colorService) : base(serviceProvider)
        {
            _colorService = colorService;
        }

        /// <summary>
        /// 获取或初始化一个 ColorDto 对象
        /// </summary>
        /// <returns></returns>
        [ApiBind]//使用 [ApiBind] 可快速创建动态 WebApi（可选）
        public async Task<AppResponseBase<Color_CaculateResponse>> GetOrInitColorDtoAsync()
        {
            return await this.GetResponseAsync<AppResponseBase<Color_CaculateResponse>, Color_CaculateResponse>(async (response, logger) =>
            {
                var dt1 = SystemTime.Now;//开始计时

                var colorDto = await _colorService.GetOrInitColor();//获取或初始化颜色参数

                var costMs = SystemTime.DiffTotalMS(dt1);//记录耗时

                Color_CaculateResponse result = new(colorDto.Red, colorDto.Green, colorDto.Blue, costMs);

                return result;
            });
        }
    }
}

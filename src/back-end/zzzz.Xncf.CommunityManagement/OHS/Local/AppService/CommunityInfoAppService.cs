﻿using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Domain.Services;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class CommunityInfoAppService : AppServiceBase
    {
        private readonly CommunityInfoService _communityInfoService;
        public CommunityInfoAppService(IServiceProvider serviceProvider, CommunityInfoService communityInfoService) : base(serviceProvider)
        {
            _communityInfoService = communityInfoService;
        }

        public async Task<PagedList<CommunityInfoDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await _communityInfoService.GetFullListAsync(t => true);
            return new PagedList<CommunityInfoDto>(
                streetList.Select(t => _communityInfoService.Mapper.Map<CommunityInfoDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}
﻿using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Domain.Services;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class CommunityUserInfoAppService : AppServiceBase
    {
        private readonly CommunityUserInfoService _communityUserInfoService;
        public CommunityUserInfoAppService(IServiceProvider serviceProvider, CommunityUserInfoService communityUserInfoService) : base(serviceProvider)
        {
            _communityUserInfoService = communityUserInfoService;
        }

        public async Task<PagedList<CommunityUserInfoDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await _communityUserInfoService.GetFullListAsync(t => true);
            return new PagedList<CommunityUserInfoDto>(
                streetList.Select(t => _communityUserInfoService.Mapper.Map<CommunityUserInfoDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}

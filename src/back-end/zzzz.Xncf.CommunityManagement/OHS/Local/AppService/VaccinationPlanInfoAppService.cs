﻿using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Domain.Services;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class VaccinationPlanInfoAppService : AppServiceBase
    {
        private readonly VaccinationPlanInfoService _vaccinationPlanInfoService;
        public VaccinationPlanInfoAppService(IServiceProvider serviceProvider, VaccinationPlanInfoService communityInfoService) : base(serviceProvider)
        {
            _vaccinationPlanInfoService = communityInfoService;
        }

        public async Task<PagedList<VaccinationPlanDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await _vaccinationPlanInfoService.GetFullListAsync(t => true);
            return new PagedList<VaccinationPlanDto>(
                streetList.Select(t => _vaccinationPlanInfoService.Mapper.Map<VaccinationPlanDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}

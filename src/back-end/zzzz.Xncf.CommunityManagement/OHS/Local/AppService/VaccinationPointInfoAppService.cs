﻿using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Domain.Services;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class VaccinationPointInfoAppService : AppServiceBase
    {
        private readonly VaccinationPointInfoService _vaccinationPointInfoService;
        public VaccinationPointInfoAppService(IServiceProvider serviceProvider, VaccinationPointInfoService vaccinationPointInfoService) : base(serviceProvider)
        {
            _vaccinationPointInfoService = vaccinationPointInfoService;
        }

        public async Task<PagedList<VaccinationPointDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await _vaccinationPointInfoService.GetFullListAsync(t => true);
            return new PagedList<VaccinationPointDto>(
                streetList.Select(t => _vaccinationPointInfoService.Mapper.Map<VaccinationPointDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}
﻿using Senparc.Ncf.Core.AppServices;
using Senparc.Ncf.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zzzz.Xncf.CommunityManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.CommunityManagement.Domain.Services;

namespace zzzz.Xncf.CommunityManagement.OHS.Local.AppService
{
    public class VaccineBatchesAppService : AppServiceBase
    {
        private readonly VaccineBatchesService _vaccineBatchesService;
        public VaccineBatchesAppService(IServiceProvider serviceProvider, VaccineBatchesService vaccineBatchesService) : base(serviceProvider)
        {
            _vaccineBatchesService = vaccineBatchesService;
        }

        public async Task<PagedList<VaccineBatchesDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await _vaccineBatchesService.GetFullListAsync(t => true);
            return new PagedList<VaccineBatchesDto>(
                streetList.Select(t => _vaccineBatchesService.Mapper.Map<VaccineBatchesDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}

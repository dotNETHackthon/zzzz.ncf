﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.Service;
using System;
using System.Threading.Tasks;

namespace zzzz.Xncf.EpidemicManagement.Areas.Pages.EpidemicManagement
{
    public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
    {

        public Index(Lazy<XncfModuleService> xncfModuleService) : base(xncfModuleService)
        {
        }
    }
}
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.Pages.QuarantinePeople;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly QuarantinePeopleService _quarantinePeopleService;
    
    public Index(Lazy<XncfModuleService> xncfModuleService, QuarantinePeopleService quarantinePeopleService) : base(xncfModuleService)
    {
        _quarantinePeopleService = quarantinePeopleService;
    }

    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<QuarantinePeopleDto> pagedList = await _quarantinePeopleService.GetOrInitQuarantinePeopleDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
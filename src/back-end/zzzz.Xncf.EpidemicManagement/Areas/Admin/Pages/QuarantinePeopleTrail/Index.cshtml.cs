using Microsoft.AspNetCore.Mvc;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.Pages.QuarantinePeopleTrail;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly QuarantinePeopleTrailService _quarantinePeopleTrailService;

    public Index(Lazy<XncfModuleService> xncfModuleService, QuarantinePeopleTrailService quarantinePeopleTrailService) : base(xncfModuleService)
    {
        _quarantinePeopleTrailService = quarantinePeopleTrailService;
    }

    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<QuarantinePeopleTrailDto> pagedList = await _quarantinePeopleTrailService.GetOrInitQuarantinePeopleTrailDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;
using zzzz.Xncf.EpidemicManagement.OHS.Local.AppService;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.Pages.QuarantineRecord;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly QuarantineRecordService _quarantineRecordService;

    public Index(Lazy<XncfModuleService> xncfModuleService, QuarantineRecordService quarantineRecordService) : base(xncfModuleService)
    {
        _quarantineRecordService = quarantineRecordService;
    }

    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<QuarantineRecordDto> pagedList = await _quarantineRecordService.GetOrInitQuarantineRecordDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
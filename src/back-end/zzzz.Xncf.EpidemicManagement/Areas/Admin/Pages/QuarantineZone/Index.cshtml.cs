using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;
using zzzz.Xncf.EpidemicManagement.OHS.Local.AppService;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.QuarantineZone.Pages;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly QuarantineZoneService _quarantineZoneService;

    public Index(Lazy<XncfModuleService> xncfModuleService, QuarantineZoneService quarantineZoneService) : base(xncfModuleService)
    {
        _quarantineZoneService = quarantineZoneService;
    }
    
    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<QuarantineZoneDto> pagedList = await _quarantineZoneService.GetOrInitQuarantineZoneDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.Pages.Street;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly StreetService _streetService;

    public Index(Lazy<XncfModuleService> xncfModuleService, StreetService streetService) : base(xncfModuleService)
    {
        _streetService = streetService;
    }

    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<StreetDto> pagedList = await _streetService.GetOrInitStreetDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
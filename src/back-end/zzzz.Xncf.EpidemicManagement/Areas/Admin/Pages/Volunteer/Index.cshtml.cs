using Microsoft.AspNetCore.Mvc;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Service;
using System;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Services;

namespace zzzz.Xncf.EpidemicManagement.Areas.Admin.Pages.Volunteer;

public class Index : Senparc.Ncf.AreaBase.Admin.AdminXncfModulePageModelBase
{
    private readonly VolunteerService _volunteerService;

    public Index(Lazy<XncfModuleService> xncfModuleService, VolunteerService volunteerService) : base(xncfModuleService)
    {
        _volunteerService = volunteerService;
    }

    public async Task<IActionResult> OnGetListAsync()
    {
        PagedList<VolunteerDto> pagedList = await _volunteerService.GetOrInitVolunteerDtoAsync();
        return Ok(new { pagedList.TotalCount, data = pagedList.ToList() });
    }
}
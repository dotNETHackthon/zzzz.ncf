using Senparc.Ncf.Core.Models;
using System;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

public class QuarantinePeopleDto : DtoBase
{
    private QuarantinePeopleDto() {}

    public string Name { get; private set; }
    public int Age { get; private set; }
    public Gender Gender { get; private set; }
    public DateTime Birthday { get; private set; }
    public string Mobile { get; private set; }
    public string Email { get; private set; }
    public string HomeAddress { get; private set; }
    public bool IsExposedInRiskArea { get; private set; }
    public bool IsFinishedRnaTest { get; private set; }
    public bool? RnaTestResult { get; private set; }
    public DateTime LastRnaTestTime { get; private set; }
}
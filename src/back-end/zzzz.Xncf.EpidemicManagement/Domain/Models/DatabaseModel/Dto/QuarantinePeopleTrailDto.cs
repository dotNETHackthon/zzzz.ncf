using Senparc.Ncf.Core.Models;
using System;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

public class QuarantinePeopleTrailDto : DtoBase
{
    private QuarantinePeopleTrailDto() {}

    public string PeopleId { get; private set; }
    public DateTime HistoryDateTime { get; private set; }
    public string StreetId { get; private set; }
    public string Address { get; private set; }
}
using Senparc.Ncf.Core.Models;
using System;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

public class QuarantineRecordDto : DtoBase
{
    private QuarantineRecordDto() {}

    public int QuarantineZoneId { get; private set; }
    public string QuarantinePeopleId { get; private set; }
    public string Mobile { get; private set; }
    public DateTime? LastRnaTestTime { get; private set; }
    public bool? RnaLastTestResult { get; private set; }
    public string VolunteerOwnerId { get; private set; }
    
    public string currentTestOwnerId { get; private set; }
    public DateTime currentTestTime { get; private set; }
    public string nextTestOwnerId { get; private set; }
    public DateTime nextTestTime { get; private set; }
    
    public float Temperature { get; private set; }
    public string HealthStatus { get; private set; }
}
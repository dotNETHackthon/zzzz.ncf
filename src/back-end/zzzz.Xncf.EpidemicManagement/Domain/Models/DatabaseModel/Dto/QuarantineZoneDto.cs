using Senparc.Ncf.Core.Models;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

public class QuarantineZoneDto : DtoBase
{
    private QuarantineZoneDto() { }
    
    public int StreetId { get; private set; }
    public string Name { get; private set; }
    public string Address { get; private set; }
    public string Remark { get; private set; }
}
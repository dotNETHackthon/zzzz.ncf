﻿using Senparc.Ncf.Core.Models;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto
{
    public class StreetDto : DtoBase
    {
        private StreetDto() { }

        public string Province { get; private set; }
        public string City { get; private set; }
        public string District { get; private set; }
        public string Name { get; private set; }
    }
}

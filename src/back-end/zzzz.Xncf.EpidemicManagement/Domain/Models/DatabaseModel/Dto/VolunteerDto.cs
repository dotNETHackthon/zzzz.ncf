using Senparc.Ncf.Core.Models;
using System;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

public class VolunteerDto : DtoBase
{
    public string Name { get; private set; }
    public Gender Gender { get; private set; }
    public DateTime Birthday { get; private set; }
    public string Mobile { get; private set; }
}
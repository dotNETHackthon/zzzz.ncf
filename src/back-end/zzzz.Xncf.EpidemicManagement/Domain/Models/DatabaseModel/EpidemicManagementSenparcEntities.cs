﻿using Microsoft.EntityFrameworkCore;
using Senparc.Ncf.XncfBase.Database;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel
{
    public class EpidemicManagementSenparcEntities : XncfDatabaseDbContext
    {
        public EpidemicManagementSenparcEntities(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
        }


        //DOT REMOVE OR MODIFY THIS LINE 请勿移除或修改本行 - Entities Point
        public DbSet<Street> Street { get; set; }
        public DbSet<QuarantineZone> QuarantineZone { get; set; }
        public DbSet<QuarantinePeople> QuarantinePeople { get; set; }
        public DbSet<QuarantinePeopleTrail> QuarantinePeopleTrail { get; set; }
        public DbSet<QuarantineRecord> QuarantineRecord { get; set; }
        public DbSet<Volunteer> Volunteer { get; set; }
        
        
        //如无特殊需需要，OnModelCreating 方法可以不用写，已经在 Register 中要求注册
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //}
    }
}

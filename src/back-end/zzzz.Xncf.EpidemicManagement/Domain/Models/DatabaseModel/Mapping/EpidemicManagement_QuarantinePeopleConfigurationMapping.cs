﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_QuarantinePeopleConfigurationMapping : ConfigurationMappingWithIdBase<QuarantinePeople, string>
    {
        public override void Configure(EntityTypeBuilder<QuarantinePeople> builder)
        {
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Mobile).IsRequired();
            builder.Property(e => e.HomeAddress).IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_QuarantinePeopleTrailConfigurationMapping : ConfigurationMappingWithIdBase<QuarantinePeopleTrail, int>
    {
        public override void Configure(EntityTypeBuilder<QuarantinePeopleTrail> builder)
        {
            builder.Property(e => e.Address).IsRequired();
        }
    }
}

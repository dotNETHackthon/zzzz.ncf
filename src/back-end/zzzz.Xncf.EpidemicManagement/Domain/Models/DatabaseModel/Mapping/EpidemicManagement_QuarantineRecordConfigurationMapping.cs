﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_QuarantineRecordConfigurationMapping : ConfigurationMappingWithIdBase<QuarantineRecord, int>
    {
        public override void Configure(EntityTypeBuilder<QuarantineRecord> builder)
        {
            builder.Property(e => e.QuarantinePeopleId).IsRequired();
            builder.Property(e => e.Mobile).IsRequired();
            builder.Property(e => e.VolunteerOwnerId).IsRequired();
            builder.Property(e => e.currentTestOwnerId).IsRequired();
            builder.Property(e => e.nextTestOwnerId).IsRequired();
            builder.Property(e => e.HealthStatus).IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_QuarantineZoneConfigurationMapping : ConfigurationMappingWithIdBase<QuarantineZone, int>
    {
        public override void Configure(EntityTypeBuilder<QuarantineZone> builder)
        {
            builder.Property(e => e.StreetId).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Address).IsRequired();
        }
    }
}

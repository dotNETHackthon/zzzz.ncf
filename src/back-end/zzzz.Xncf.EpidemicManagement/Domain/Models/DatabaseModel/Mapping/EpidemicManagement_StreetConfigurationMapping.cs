﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_StreetConfigurationMapping : ConfigurationMappingWithIdBase<Street, int>
    {
        public override void Configure(EntityTypeBuilder<Street> builder)
        {
            builder.Property(e => e.Province).IsRequired();
            builder.Property(e => e.City).IsRequired();
            builder.Property(e => e.District).IsRequired();
            builder.Property(e => e.Name).IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Senparc.Ncf.Core.Models.DataBaseModel;
using Senparc.Ncf.XncfBase.Attributes;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Mapping
{
    [XncfAutoConfigurationMapping]
    public class EpidemicManagement_VolunteerConfigurationMapping : ConfigurationMappingWithIdBase<Volunteer, string>
    {
        public override void Configure(EntityTypeBuilder<Volunteer> builder)
        {
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Mobile).IsRequired();
        }
    }
}

using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(QuarantinePeople))]
[Serializable]
public class QuarantinePeople : EntityBase<string>
{
    private QuarantinePeople() {}

    public QuarantinePeople(string id,
                            string name,
                            int age,
                            Gender gender,
                            DateTime birthday,
                            string mobile,
                            string email,
                            string homeAddress,
                            bool isExposedInRiskArea,
                            bool isFinishedRnaTest,
                            bool? rnaTestResult,
                            DateTime lastRnaTestTime)
    {
        Id = id;
        Name = name;
        Age = age;
        Gender = gender;
        Birthday = birthday;
        Mobile = mobile;
        Email = email;
        HomeAddress = homeAddress;
        IsExposedInRiskArea = isExposedInRiskArea;
        IsFinishedRnaTest = isFinishedRnaTest;
        RnaTestResult = rnaTestResult;
        LastRnaTestTime = lastRnaTestTime;
    }

    public string Name { get; set; }
    public int Age { get; set; }
    public Gender Gender { get; set; }
    public DateTime Birthday { get; set; }
    public string Mobile { get; set; }
    public string Email { get; set; }
    public string HomeAddress { get; set; }
    public bool IsExposedInRiskArea { get; set; }
    public bool IsFinishedRnaTest { get; set; }
    public bool? RnaTestResult { get; set; }
    public DateTime LastRnaTestTime { get; set; }
}
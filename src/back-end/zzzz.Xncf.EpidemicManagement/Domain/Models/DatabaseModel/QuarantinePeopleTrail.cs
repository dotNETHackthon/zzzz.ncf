using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(QuarantinePeopleTrail))]
[Serializable]
public class QuarantinePeopleTrail : EntityBase<int>
{
    public QuarantinePeopleTrail() {}

    public QuarantinePeopleTrail(string peopleId, DateTime historyDateTime, int streetId, string address)
    {
        PeopleId = peopleId;
        HistoryDateTime = historyDateTime;
        StreetId = streetId;
        Address = address;
    }

    public string PeopleId { get; set; }
    public DateTime HistoryDateTime { get; set; }
    public int StreetId { get; set; }
    public string Address { get; set; }
}
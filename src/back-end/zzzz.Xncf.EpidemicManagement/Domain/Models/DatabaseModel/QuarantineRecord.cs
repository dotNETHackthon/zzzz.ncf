using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(QuarantineRecord))]
[Serializable]
public class QuarantineRecord : EntityBase<int>
{
    public QuarantineRecord() {}

    public QuarantineRecord(int quarantineZoneId, 
                            string quarantinePeopleId, 
                            string mobile, 
                            DateTime? lastRnaTestTime,
                            bool? rnaLastTestResult, 
                            string volunteerOwnerId, 
                            string currentTestOwnerId, 
                            DateTime currentTestTime, 
                            string nextTestOwnerId, 
                            DateTime nextTestTime,
                            float temperature,
                            string healthStatus)
    {
        QuarantineZoneId = quarantineZoneId;
        QuarantinePeopleId = quarantinePeopleId;
        Mobile = mobile;
        LastRnaTestTime = lastRnaTestTime;
        RnaLastTestResult = rnaLastTestResult;
        VolunteerOwnerId = volunteerOwnerId;
        this.currentTestOwnerId = currentTestOwnerId;
        this.currentTestTime = currentTestTime;
        this.nextTestOwnerId = nextTestOwnerId;
        this.nextTestTime = nextTestTime;
        Temperature = temperature;
        HealthStatus = healthStatus;
    }

    public int QuarantineZoneId { get; set; }
    public string QuarantinePeopleId { get; set; }
    public string Mobile { get; set; }
    public DateTime? LastRnaTestTime { get; set; }
    public bool? RnaLastTestResult { get; set; }
    public string VolunteerOwnerId { get; set; }
    
    public string currentTestOwnerId { get; set; }
    public DateTime currentTestTime { get; set; }
    public string nextTestOwnerId { get; set; }
    public DateTime nextTestTime { get; set; }
    
    public float Temperature { get; set; }
    public string HealthStatus { get; set; }
}
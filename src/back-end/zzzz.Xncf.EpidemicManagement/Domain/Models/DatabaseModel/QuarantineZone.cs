using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(QuarantineZone))]//必须添加前缀，防止全系统中发生冲突
[Serializable]
public class QuarantineZone : EntityBase<int>
{
    
    private QuarantineZone() { }
    
    public QuarantineZone(int streetId, string name, string address, string remark="")
    {
        StreetId = streetId;
        Name = name;
        Address = address;
        Remark = remark;
    }

    public int StreetId { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string Remark { get; set; }
}
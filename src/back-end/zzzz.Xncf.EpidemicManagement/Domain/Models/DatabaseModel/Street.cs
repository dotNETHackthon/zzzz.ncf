using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(Street))]
[Serializable]
public class Street : EntityBase<int>
{
    private Street() { }

    public Street(string province, string city, string district, string name)
    {
        Province = province;
        City = city;
        District = district;
        Name = name;
    }

    public string Province { get; private set; }
    public string City { get; private set; }
    public string District { get; private set; }
    public string Name { get; private set; }
}

using Senparc.Ncf.Core.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;

[Table(Register.DATABASE_PREFIX + nameof(Volunteer))]//必须添加前缀，防止全系统中发生冲突
[Serializable]
public class Volunteer : EntityBase<string>
{
    private Volunteer() {}

    public Volunteer(string id, string name, Gender gender, DateTime birthday, string mobile)
    {
        Id = id;
        Name = name;
        Gender = gender;
        Birthday = birthday;
        Mobile = mobile;
    }

    public string Name { get; set; }
    public Gender Gender { get; set; }
    public DateTime Birthday { get; set; }
    public string Mobile { get; set; }
}
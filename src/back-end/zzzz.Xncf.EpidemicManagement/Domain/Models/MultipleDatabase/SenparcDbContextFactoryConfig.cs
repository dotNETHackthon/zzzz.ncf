﻿using System;
using System.IO;

namespace zzzz.Xncf.EpidemicManagement.Models
{
    /// <summary>
    /// SenparcDbContextFactory 的公共配置
    /// </summary>
    public static class SenparcDbContextFactoryConfig
    {
        private static string _rootDictionaryPath = null;

        /// <summary>
        /// 用于寻找 App_Data 文件夹，从而找到数据库连接字符串配置信息
        /// </summary>
        public static string RootDictionaryPath
        {
            get
            {
                if (_rootDictionaryPath == null)
                {
                    var projectPath = Path.GetFullPath(string.Join(Path.DirectorySeparatorChar, "..", "..", "..") + Path.DirectorySeparatorChar, AppContext.BaseDirectory);//项目根目录

                    var webPath = Path.GetFullPath(Path.Combine("..", "Senparc.Web"), projectPath);
                    if (Directory.Exists(webPath))
                    {
                        _rootDictionaryPath = webPath;//优先使用Web统一配置
                    }
                    _rootDictionaryPath = projectPath;
                }
                return _rootDictionaryPath;
            }
        }
    }
}

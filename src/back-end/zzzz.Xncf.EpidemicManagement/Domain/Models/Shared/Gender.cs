namespace zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

public enum Gender
{
    Male,
    Female
}
﻿using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class QuarantinePeopleService : ServiceBase<QuarantinePeople>
    {
        public QuarantinePeopleService(IRepositoryBase<QuarantinePeople> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<PagedList<QuarantinePeopleDto>> GetOrInitQuarantinePeopleDtoAsync()
        {
            var QuarantinePeopleList = await this.GetFullListAsync(t => true);

            string GenerateId()
            {
                return new Random(Guid.NewGuid().GetHashCode()).Next().GetHashCode().ToString();
            }

            if (QuarantinePeopleList.TotalCount == 0)
            {
                var initQuarantinePeopleList = new List<QuarantinePeople> {
                    new(GenerateId(), "李开富", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "王子久", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "刘永生", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "刘宝瑞", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "关玉和", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "王仁兴", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "李际泰", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "罗元发", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "刘造时", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "刘乃超", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "刘长胜", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "张成基", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "张国柱", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "张志远", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "张广才", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "吕德榜", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "吕文达", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "吴家栋", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "吴国梁", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "吴立功", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "李大江", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()),
                    new(GenerateId(), "张石山", Generator.RandomInt(20, 60), Generator.RandomEnum<Gender>(), new DateTime(Generator.RandomInt(1961, 1991),Generator.RandomInt(1,12),Generator.RandomInt(1,28)), "132****" + Generator.RandomInt(1001, 9988), Generator.RandomString(12) + "@gmail.com", "北京市", Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomInt() % 2 == 0, Generator.RandomDate()), };

                await this.SaveObjectListAsync(initQuarantinePeopleList).ConfigureAwait(false);
                QuarantinePeopleList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<QuarantinePeopleDto>(
                QuarantinePeopleList.Select(t => this.Mapper.Map<QuarantinePeopleDto>(t)).ToList(),
                QuarantinePeopleList.PageIndex,
                QuarantinePeopleList.PageCount,
                QuarantinePeopleList.TotalCount);
        }
    }
}

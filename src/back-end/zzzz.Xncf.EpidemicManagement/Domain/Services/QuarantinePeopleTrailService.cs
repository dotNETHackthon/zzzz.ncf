﻿using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class QuarantinePeopleTrailService : ServiceBase<QuarantinePeopleTrail>
    {
        public QuarantinePeopleTrailService(IRepositoryBase<QuarantinePeopleTrail> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<PagedList<QuarantinePeopleTrailDto>> GetOrInitQuarantinePeopleTrailDtoAsync()
        {
            var QuarantinePeopleTrailList = await this.GetFullListAsync(t => true);
            if (QuarantinePeopleTrailList.TotalCount == 0)
            {
                var initQuarantinePeopleTrailList = Generator.RandomList<QuarantinePeopleTrail>(200);

                await this.SaveObjectListAsync(initQuarantinePeopleTrailList).ConfigureAwait(false);
                QuarantinePeopleTrailList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<QuarantinePeopleTrailDto>(
                QuarantinePeopleTrailList.Select(t => this.Mapper.Map<QuarantinePeopleTrailDto>(t)).ToList(),
                QuarantinePeopleTrailList.PageIndex,
                QuarantinePeopleTrailList.PageCount,
                QuarantinePeopleTrailList.TotalCount);
        }
    }
}

﻿using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class QuarantineRecordService : ServiceBase<QuarantineRecord>
    {
        public QuarantineRecordService(IRepositoryBase<QuarantineRecord> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<PagedList<QuarantineRecordDto>> GetOrInitQuarantineRecordDtoAsync()
        {
            var QuarantineRecordList = await this.GetFullListAsync(t => true);
            if (QuarantineRecordList.TotalCount == 0)
            {
                var initQuarantineRecordList = Generator.RandomList<QuarantineRecord>(200).Select(t =>
                {
                    t.Temperature = Generator.RandomInt(350, 400) / 10.0f;
                    return t;
                });

                await this.SaveObjectListAsync(initQuarantineRecordList).ConfigureAwait(false);
                QuarantineRecordList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<QuarantineRecordDto>(
                QuarantineRecordList.Select(t => this.Mapper.Map<QuarantineRecordDto>(t)).ToList(),
                QuarantineRecordList.PageIndex,
                QuarantineRecordList.PageCount,
                QuarantineRecordList.TotalCount);
        }
    }
}

﻿using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class QuarantineZoneService : ServiceBase<QuarantineZone>
    {
        public QuarantineZoneService(IRepositoryBase<QuarantineZone> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<PagedList<QuarantineZoneDto>> GetOrInitQuarantineZoneDtoAsync()
        {
            var QuarantineZoneList = await this.GetFullListAsync(t => true);
            if (QuarantineZoneList.TotalCount == 0)
            {
                var initQuarantineZoneList = new List<QuarantineZone> {
                    new(1, "东直门居然大厦隔离点", "北京市东城区东直门内大街78号", "物资充裕"),
                    new(2, "望京SOHO隔离点", "北京市东城区东直门内大街78号", "物资充裕"),
                    new(3, "通州北关隔离点", "北京市通州新华大街23号院", "物资充裕"),
                    new(4, "西直门隔离点", "北京市西直门动物园东", "物资充裕"),
                    new(2, "海淀隔离点", "北京市海淀黄庄32号楼", "物资充裕"),
                    new(5, "昌平隔离点", "北京市后沙峪南", "物资充裕"),
                    new(6, "朝阳区隔离点", "北京市朝阳区针织路23号楼", "物资充裕"),
                    new(7, "西城区隔离点", "北京市西城区复兴门南大街2号", "物资充裕"),
                    new(8, "丰台区隔离点", "北京市丰台区海鹰路1号1幢11层006", "物资充裕"),
                };

                await this.SaveObjectListAsync(initQuarantineZoneList).ConfigureAwait(false);
                QuarantineZoneList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<QuarantineZoneDto>(
                QuarantineZoneList.Select(t => this.Mapper.Map<QuarantineZoneDto>(t)).ToList(),
                QuarantineZoneList.PageIndex,
                QuarantineZoneList.PageCount,
                QuarantineZoneList.TotalCount);
        }
    }
}

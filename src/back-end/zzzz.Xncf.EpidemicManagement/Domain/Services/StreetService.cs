﻿using Senparc.Ncf.Core.Enums;
using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using zzzz.Xncf.EpidemicManagement.Domain.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class StreetService : ServiceBase<Street>
    {
        public StreetService(IRepositoryBase<Street> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<StreetDto> CreateNewStreet(Street street)
        {
            await base.SaveObjectAsync(street).ConfigureAwait(false);
            StreetDto streetDto = base.Mapper.Map<StreetDto>(street);
            return streetDto;
        }
        
        public async Task<PagedList<StreetDto>> GetOrInitStreetDtoAsync()
        {
            var streetList = await this.GetFullListAsync(t => true);
            if (streetList.TotalCount == 0)
            {
                var initStreetList = new List<Street> {
                    new("北京", "北京", "东城区", "东直门外大街"),
                    new("北京", "北京", "西城区", "西直门外大街137号"),
                    new("北京", "北京", "崇文区", "天坛内东里7号"),
                    new("北京", "北京", "宣武区", "宣武医院"),
                    new("北京", "北京", "朝阳区", "建国路"),
                    new("北京", "北京", "丰台区", "北京南站"),
                    new("北京", "北京", "石景山区", "八宝山地铁站"),
                    new("北京", "北京", "海淀区", "新建宫门路19号"),
                    new("北京", "北京", "门头沟区", "沿河城村幽州大峡谷"),
                    new("北京", "北京", "房山区", "韩村河镇天开村村西天开水库旁"),
                    new("北京", "北京", "通州区", "张凤路"),
                    new("北京", "北京", "顺义区", "木燕路灵石59号"),
                    new("北京", "北京", "昌平区", "十三陵镇昌赤路七孔桥东侧"),
                    new("北京", "北京", "大兴区", "大兴机场"),
                    new("北京", "北京", "怀柔区", "渤海镇慕田峪村"),
                    new("北京", "北京", "平谷区", "玻璃台村"),
                    new("北京", "北京", "密云县", "石城镇石城村密关路"),
                    new("北京", "北京", "延庆县", "张山营镇北部松山自然保护区"),
                };

                await this.SaveObjectListAsync(initStreetList).ConfigureAwait(false);
                streetList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<StreetDto>(
                streetList.Select(t => this.Mapper.Map<StreetDto>(t)).ToList(),
                streetList.PageIndex,
                streetList.PageCount,
                streetList.TotalCount);
        }
    }
}

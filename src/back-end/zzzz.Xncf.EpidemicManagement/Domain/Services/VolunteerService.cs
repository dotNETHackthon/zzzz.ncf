﻿using Senparc.Ncf.Core.Models;
using Senparc.Ncf.Repository;
using Senparc.Ncf.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel;
using zzzz.Xncf.EpidemicManagement.Domain.Models.DatabaseModel.Dto;
using zzzz.Xncf.EpidemicManagement.Domain.Models.Shared;

namespace zzzz.Xncf.EpidemicManagement.Domain.Services
{
    public class VolunteerService : ServiceBase<Volunteer>
    {
        public VolunteerService(IRepositoryBase<Volunteer> repo, IServiceProvider serviceProvider)
            : base(repo, serviceProvider)
        {
        }

        public async Task<PagedList<VolunteerDto>> GetOrInitVolunteerDtoAsync()
        {
            var VolunteerList = await this.GetFullListAsync(t => true);
            if (VolunteerList.TotalCount == 0)
            {
                var initVolunteerList = new List<Volunteer> {
                    new(Generator.RandomString(17), "彭万里", Gender.Male, new DateTime(1987, 3, 23), "176****2937"),
                    new(Generator.RandomString(17), "高大山", Gender.Female, new DateTime(1987, 3, 23), "136****8265"),
                    new(Generator.RandomString(17), "谢大海", Gender.Male, new DateTime(1987, 3, 23), "186****4836"),
                    new(Generator.RandomString(17), "马宏宇", Gender.Female, new DateTime(1987, 3, 23), "185****4483"),
                    new(Generator.RandomString(17), "林莽", Gender.Female, new DateTime(1987, 3, 23), "138****4562"),
                    new(Generator.RandomString(17), "李际泰", Gender.Male, new DateTime(1987, 3, 23), "132****6731"),
                    new(Generator.RandomString(17), "黄强辉", Gender.Male, new DateTime(1987, 3, 23), "188****3732"),
                    new(Generator.RandomString(17), "章汉夫", Gender.Female, new DateTime(1987, 3, 23), "191****4489"),
                    new(Generator.RandomString(17), "范长江", Gender.Male, new DateTime(1987, 3, 23), "176****0987"),
                    new(Generator.RandomString(17), "林君雄", Gender.Female, new DateTime(1987, 3, 23), "186****8532"),
                    new(Generator.RandomString(17), "谭平山", Gender.Female, new DateTime(1987, 3, 23), "166****9372"),
                    new(Generator.RandomString(17), "朱希亮", Gender.Male, new DateTime(1987, 3, 23), "185****8373"),
                    new(Generator.RandomString(17), "李四光", Gender.Female, new DateTime(1987, 3, 23), "136****4273"),
                    new(Generator.RandomString(17), "甘铁生", Gender.Male, new DateTime(1987, 3, 23), "135****4836"),
                    new(Generator.RandomString(17), "张伍绍", Gender.Male, new DateTime(1987, 3, 23), "139****3826"),
                };

                await this.SaveObjectListAsync(initVolunteerList).ConfigureAwait(false);
                VolunteerList = await this.GetFullListAsync(t => true);
            }

            return new PagedList<VolunteerDto>(
                VolunteerList.Select(t => this.Mapper.Map<VolunteerDto>(t)).ToList(),
                VolunteerList.PageIndex,
                VolunteerList.PageCount,
                VolunteerList.TotalCount);
        }
    }
}
